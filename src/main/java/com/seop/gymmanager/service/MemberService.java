package com.seop.gymmanager.service;

import com.seop.gymmanager.entity.Member;
import com.seop.gymmanager.model.MemberItem;
import com.seop.gymmanager.model.MemberResponse;
import com.seop.gymmanager.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public void setMember(
            String gender,
            String name,
            String phone,
            Integer period,
            Integer presentWeight,
            Integer targetWeight,
            Boolean usedLocker){

        Member addData = new Member();

        addData.setGender(gender);
        addData.setName(name);
        addData.setPhone(phone);
        addData.setPeriod(period);
        addData.setPresentWeight(presentWeight);
        addData.setTargetWeight(targetWeight);
        addData.setDataTitleLast(LocalDateTime.now());
        addData.setUsedLocker(usedLocker);

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<Member> originData = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for(Member item : originData) {
            MemberItem addItem = new MemberItem();
            addItem.setId(item.getId());
            addItem.setName(item.getName());
            addItem.setPhone(item.getPhone());
            addItem.setPeriod(item.getPeriod() + "개월");
            addItem.setUsedLocker(item.getUsedLocker());

            result.add(addItem);
        }
        return result;
    }

    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();

        MemberResponse result = new MemberResponse();

        result.setId(originData.getId());
        result.setName(originData.getName());
        result.setPhone(originData.getPhone());
        result.setPeriod(originData.getPeriod() + "개월");
        result.setPresentWeight(originData.getPresentWeight() + "kg");
        result.setTargetWeight(originData.getTargetWeight() + "kg");
        result.setLossWeight((originData.getPresentWeight() - originData.getTargetWeight()) + "kg");
        result.setUsedLocker(originData.getUsedLocker());
        result.setDataTitleLast(originData.getDataTitleLast());
        result.setGender(originData.getGender());

        return result;
    }
}
