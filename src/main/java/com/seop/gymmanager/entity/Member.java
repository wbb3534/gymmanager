package com.seop.gymmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 4)
    private String gender; // 성별
    @Column(nullable = false, length = 20)
    private String name; // 이름
    @Column(nullable = false, length = 13)
    private String phone; // 전화번호
    @Column(nullable = false)
    private Integer period; // 이용기간
    @Column(nullable = false)
    private Integer presentWeight; // 현재 몸무게
    @Column(nullable = false)
    private Integer targetWeight; // 목표 몸무게
    @Column(nullable = false)
    private Boolean usedLocker; // 락커 사용유무
    @Column(nullable = false)
    private LocalDateTime dataTitleLast; // 오늘 날짜 시간
}
