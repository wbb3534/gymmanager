package com.seop.gymmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class MemberResponse {
    private Long id;
    private String gender;
    private String name;
    private String phone;
    private String period;
    private String lossWeight;
    private String presentWeight;
    private String targetWeight;
    private Boolean usedLocker;
    private LocalDateTime dataTitleLast;
}
