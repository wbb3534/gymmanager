package com.seop.gymmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class MemberRequest {
    @NotNull
    @Length(min = 1, max = 4)
    private String gender;
    @NotNull
    @Length(min = 2, max = 20)
    private String name;
    @NotNull
    @Length(min = 11, max = 13)
    private String phone;
    @NotNull
    private Integer period;
    @NotNull
    private Integer presentWeight;
    @NotNull
    private Integer targetWeight;
    @NotNull
    private Boolean usedLocker;
}
