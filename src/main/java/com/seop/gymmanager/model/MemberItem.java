package com.seop.gymmanager.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MemberItem {
    private Long id;
    private String name;
    private String phone;
    private String period;
    private Boolean usedLocker;
}
