package com.seop.gymmanager.controller;

import com.seop.gymmanager.model.MemberItem;
import com.seop.gymmanager.model.MemberRequest;
import com.seop.gymmanager.model.MemberResponse;
import com.seop.gymmanager.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/member")
@RequiredArgsConstructor
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/create")
    public String setMemberData(@RequestBody @Valid MemberRequest request) {
        memberService.setMember(
                request.getGender(),
                request.getName(),
                request.getPhone(),
                request.getPeriod(),
                request.getPresentWeight(),
                request.getTargetWeight(),
                request.getUsedLocker()
        );
        return "성공";
    }

    @GetMapping("/members")
    public List<MemberItem> getMembers() {
        List<MemberItem> result = memberService.getMembers();
        return result;
    }

    @GetMapping("/member/{id}")
    public MemberResponse getMember(@PathVariable long id) {
        MemberResponse result = memberService.getMember(id);
        return result;
    }
}
